﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using MoreMountains.NiceVibrations;

public class Movement : MonoBehaviour
{
    public enum Type { Player, Ally, Enemy };
    public Type aiType = Type.Player;
    private float _moveSmoothing = 0.3f;
    private NavMeshAgent _agent;
    Transform stack;

    [Space]
    public float playerSpeed = 6f;
    public float allySpeed = 5.5f;
    public float enemySpeed = 3f;

    Vector2 startMouse;
    bool moving;
    Camera cam;
    float hor;
    float ver;

    public List<Food> foods;
    Vector3 stackPos;
    int foodInt;

    public bool marked;
    Movement myMark;
    Animator anim;
    bool hit;
    bool hittable = true;
    bool canBank = true;
    bool goingToBank = false;

    private void Awake()
    {
        stack = transform.GetChild(0);
        cam = Camera.main;
        _agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();

        switch (aiType)
        {
            case Type.Player:
                _agent.speed = playerSpeed;
                break;

            case Type.Ally:
                _agent.speed = allySpeed;
                break;

            case Type.Enemy:
                _agent.speed = enemySpeed;
                break;
        }
    }

    void Start()
    {
        _agent.updatePosition = false;

        if (aiType == Type.Ally)
        {
            Invoke(nameof(UpdateAllyDestination), 0);
            _agent.updatePosition = true;

        }
        if (aiType == Type.Enemy)
        {
            Invoke(nameof(UpdateEnemyDestination), 0);
            _agent.updatePosition = true;

        }
    }

    void Update()
    {
        switch (aiType)
        {
            case Type.Player:

                if (foods.Count == 0) anim.SetBool("Carry", false); else anim.SetBool("Carry", true);
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Explode();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    
                    Vector3 a = cam.ScreenToViewportPoint(Input.mousePosition);
                    startMouse = a;
                }
                if(Input.GetMouseButton(0))
                {
                    if(Vector2.Distance(cam.ScreenToViewportPoint(Input.mousePosition), startMouse) > 0.05f)
                    {
                        moving = true;
                    }
                }
                else
                {
                    moving = false;
                    hor = 0;
                    ver = 0;

                }

                if (moving)
                {
                    anim.SetBool("Run", true);
                    if (foods.Count == 0) anim.SetBool("RunCarry", false); else anim.SetBool("RunCarry", true);

                    Vector2 cur = cam.ScreenToViewportPoint(Input.mousePosition);

                    if (Vector2.Distance(cur, startMouse) > 0.005f)
                    {

                        Vector2 a = new Vector2(cur.x - startMouse.x, cur.y - startMouse.y).normalized;
                        hor = a.x;
                        ver = a.y;
                        Vector2 n = cur - (cur - startMouse);
                        startMouse = n;

                    }
                    
                }
                else
                {
                    anim.SetBool("Run", false);
                    anim.SetBool("RunCarry", false);
                }

                Vector3 movement = new Vector3(hor, 0, ver);

                if (hit)
                    movement = Vector3.zero;

                    _agent.Move(movement * Time.deltaTime * _agent.speed);


                transform.position = Vector3.Lerp(this.transform.position, _agent.nextPosition, 1);
                transform.LookAt(transform.position + movement);

                if (Vector3.Distance(transform.position, Bank.instance.transform.position) < 5)
                    Bankk();
                break;

            case Type.Ally:

                if(_agent.velocity.magnitude > 0)
                {
                    anim.SetBool("Run", true);
                    if (foods.Count > 0)
                        anim.SetBool("RunCarry", true);
                    else
                        anim.SetBool("RunCarry", false);
                }
                else
                {
                    anim.SetBool("Run", false);
                    anim.SetBool("RunCarry", false);
                }
                if (foods.Count > 0)
                {
                    anim.SetBool("Carry", true);
                }
                else
                    anim.SetBool("Carry", false);

                    _agent.isStopped = hit;

                if (Vector3.Distance(transform.position, Bank.instance.transform.position) < 5)
                {
                    Bankk();
                    goingToBank = false;
                }


                break;

            case Type.Enemy:

                if (_agent.velocity.magnitude > 0)
                    anim.SetBool("Run", true);
                else
                    anim.SetBool("Run", false);

                    break;
        }

    }

    void Bankk()
    {
        if (canBank)
        {
            FoodFly();
            canBank = false;
            foodInt = 0;
            stackPos = Vector3.zero;
        }
    }

    void FoodFly()
    {
        if(foods.Count>0)
        {
            print("GO!");
            foods[foods.Count - 1].GoToBank();
            foods.RemoveAt(foods.Count - 1);
            Invoke(nameof(FoodFly), 0.1f);
        }
    }

    void UpdateAllyDestination()
    {
        Invoke(nameof(UpdateAllyDestination), Random.Range(0f, 0.1f));

        if (!goingToBank)
        {
            Food[] foods = FindObjectsOfType<Food>();
            List<Transform> foodTransforms = new List<Transform>();
            for (int i = 0; i < foods.Length; i++)
            {
                if (foods[i].collectable)
                    foodTransforms.Add(foods[i].transform);
            }

            SetDestination(GetClosestTransform(foodTransforms));
        }
        else
        {
            SetDestination(Bank.instance.transform);
        }
    }

    void UpdateEnemyDestination()
    {
        Invoke(nameof(UpdateEnemyDestination), Random.Range(0.3f, 1.2f));

        Movement[] allies = FindObjectsOfType<Movement>();
        List<Transform> allyTransforms = new List<Transform>();

        for (int i = 0; i < allies.Length; i++)
        {
            if(allies[i].aiType != Type.Enemy)
            {
                if(allies[i] == myMark)
                    allyTransforms.Add(allies[i].transform);

                else if(!allies[i].marked)
                    allyTransforms.Add(allies[i].transform);

                //otherwise dont add it

                

            }
        }

        Transform ally = GetClosestTransform(allyTransforms);

        if (myMark != null && ally.GetComponent<Movement>() != myMark)
        {
            myMark.marked = false;
            myMark = null;
        }

        ally.GetComponent<Movement>().marked = true;
        myMark = ally.GetComponent<Movement>();

        SetDestination(ally);

    }

    void SetDestination(Transform t)
    {
        if (t != null)
            _agent.SetDestination(t.position);

        else
            _agent.SetDestination(transform.position);
    }



    Transform GetClosestTransform(List<Transform> enemies)
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in enemies)
        {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }
        return bestTarget;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(aiType == Type.Player || aiType == Type.Ally)
        {
            Food f = collision.gameObject.GetComponent<Food>();
            if (f)
            {
                if(f.collectable && !f.inStack)
                {
                    canBank = true;
                    foods.Add(f);
                    f.Collect(stack, stackPos, foodInt, this);
                    stackPos += Vector3.up * 0.5f;
                    foodInt++;
                    f.transform.SetParent(stack);

                    if(aiType == Type.Player)
                        GameManager.instance.Vibrate(HapticTypes.LightImpact);

                    if (aiType == Type.Ally)
                        if (Random.value > 1 - (foods.Count * 0.1f))
                            goingToBank = true;
                }
            }
        }

        Movement m = collision.gameObject.GetComponent<Movement>();
        if (m)
        {
            if (aiType == Type.Enemy)
            {
                if (m.aiType != Type.Enemy)
                {
                    m.Explode();
                }
            }
        }
    }

    void Explode()
    {
        if (hittable)
        {
            hit = true;
            hittable = false;
            anim.SetBool("Hit", hit);
            Invoke(nameof(BecomeHittable), 4);
            Invoke(nameof(Unhit), 1.5f);
            GameManager.instance.Vibrate(HapticTypes.Warning);

            foreach (Food f in foods)
            {
                f.Explode();
                f.transform.parent = null;
            }
            foods.Clear();
            foodInt = 0;
            stackPos = Vector3.zero;
        }
    }

    void Unhit()
    {
        hit = false;
        anim.SetBool("Hit", hit);
    }

    void BecomeHittable()
    {
        hittable = true;
    }
}
