﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class Food : MonoBehaviour
{
    Rigidbody rb;
    public Collider col;
    public Collider trig;
    public bool collectable;
    public bool inStack;
    public Vector3 stackPos = new Vector3(1, 1, 1);
    Transform stack;
    public int foodInt;

    bool moving;
    Transform graphics;
    public GameObject burstPrefab;

    public GameObject[] foods;

    public bool goToBank;



    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        graphics = transform.GetChild(0);
        foods[Random.Range(0, foods.Length)].SetActive(true);
        transform.rotation = Random.rotation;
    }

    public void Collect(Transform s, Vector3 sp, int f, Movement m)
    {
        rb.useGravity = false;

        col.enabled = false;
        collectable = false;
        stackPos = sp;
        stack = s;
        inStack = true;
        foodInt = f;

        SizeBurst();
        Instantiate(burstPrefab, transform.position, Quaternion.identity);

    }

    void SizeBurst()
    {
        graphics.localScale = Vector3.one * 2.2f;
    }

    public void GoToBank()
    {
        goToBank = true;
        transform.parent = null;
        collectable = false;
        inStack = true;
    }
    private void Update()
    {
        graphics.localScale = Vector3.Lerp(graphics.localScale, Vector3.one, Time.deltaTime * 10);

        if (goToBank)
        {
            transform.position = Vector3.MoveTowards(transform.position, Bank.instance.transform.position, Time.deltaTime * 15);
            if(Vector3.Distance(transform.position, Bank.instance.transform.position) < 0.1f)
            {
                Bank.instance.Deposit();
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (stackPos != Vector3.one && inStack)
            {
                if (Vector3.Distance(transform.position, stack.position + stackPos) > 0.05f)
                {
                    transform.position = Vector3.MoveTowards(transform.position, stack.position + stackPos, Time.deltaTime * 12);
                }
                else
                {
                    transform.position = stack.position + stackPos;
                }
            }
        }
    }

    public void Explode()
    {
        rb.useGravity = true;
        col.enabled = true;
        rb.isKinematic = false;
        float xx = 4, yy = 6, zz = 4;
        Vector3 randVec = new Vector3(Random.Range(-xx, xx), Random.Range(6, 12), Random.Range(-zz, zz));
        rb.AddForce(randVec, ForceMode.Impulse);
        rb.AddTorque(Random.insideUnitCircle * 10, ForceMode.Impulse);
        Invoke("CollectableTrue", 1f);
        stackPos = Vector3.one;
        inStack = false;
        moving = false;
    }

    void CollectableTrue()
    {
        collectable = true;
        col.enabled = true;
        
    }
}
