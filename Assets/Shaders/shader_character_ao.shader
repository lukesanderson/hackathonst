// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:8020,x:33882,y:32650,varname:node_8020,prsc:2|diff-6853-RGB,emission-8203-OUT,custl-304-OUT;n:type:ShaderForge.SFN_Tex2d,id:6853,x:33223,y:32645,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_6853,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5228-UVOUT;n:type:ShaderForge.SFN_LightVector,id:9202,x:32128,y:32917,varname:node_9202,prsc:2;n:type:ShaderForge.SFN_Dot,id:2154,x:32445,y:33013,varname:node_2154,prsc:2,dt:0|A-9202-OUT,B-2542-OUT;n:type:ShaderForge.SFN_NormalVector,id:2542,x:32128,y:33064,prsc:2,pt:False;n:type:ShaderForge.SFN_Step,id:2711,x:32635,y:33084,varname:node_2711,prsc:2|A-4423-OUT,B-2154-OUT;n:type:ShaderForge.SFN_Vector1,id:4423,x:32445,y:33206,varname:node_4423,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:9878,x:32686,y:32912,ptovrint:False,ptlb:Lighting,ptin:_Lighting,varname:node_9878,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Max,id:1499,x:32878,y:32956,varname:node_1499,prsc:2|A-9878-OUT,B-2711-OUT;n:type:ShaderForge.SFN_Multiply,id:3145,x:33085,y:32872,varname:node_3145,prsc:2|A-1499-OUT,B-9022-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9022,x:32848,y:33290,ptovrint:False,ptlb:Brightness,ptin:_Brightness,varname:node_9022,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:7740,x:33258,y:33150,varname:node_7740,prsc:2|A-3145-OUT,B-2154-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:304,x:33498,y:32881,ptovrint:False,ptlb:Toon on/off,ptin:_Toononoff,varname:node_304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3145-OUT,B-7740-OUT;n:type:ShaderForge.SFN_Multiply,id:8203,x:33496,y:32530,varname:node_8203,prsc:2|A-836-RGB,B-6853-RGB;n:type:ShaderForge.SFN_Tex2d,id:836,x:33255,y:32455,ptovrint:False,ptlb:AO,ptin:_AO,varname:node_836,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-1308-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:1308,x:32961,y:32429,varname:node_1308,prsc:2,uv:1,uaff:False;n:type:ShaderForge.SFN_TexCoord,id:5228,x:32961,y:32629,varname:node_5228,prsc:2,uv:0,uaff:False;proporder:6853-9878-9022-304-836;pass:END;sub:END;*/

Shader "Custom/shader_character_ao" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _Lighting ("Lighting", Float ) = 0
        _Brightness ("Brightness", Float ) = 0
        [MaterialToggle] _Toononoff ("Toon on/off", Float ) = 0
        _AO ("AO", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _AO; uniform float4 _AO_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _Lighting)
                UNITY_DEFINE_INSTANCED_PROP( float, _Brightness)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _Toononoff)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
////// Emissive:
                float4 _AO_var = tex2D(_AO,TRANSFORM_TEX(i.uv1, _AO));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 emissive = (_AO_var.rgb*_Texture_var.rgb);
                float _Lighting_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Lighting );
                float node_2154 = dot(lightDirection,i.normalDir);
                float _Brightness_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Brightness );
                float node_3145 = (max(_Lighting_var,step(0.0,node_2154))*_Brightness_var);
                float _Toononoff_var = lerp( node_3145, (node_3145+node_2154), UNITY_ACCESS_INSTANCED_PROP( Props, _Toononoff ) );
                float3 finalColor = emissive + float3(_Toononoff_var,_Toononoff_var,_Toononoff_var);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _AO; uniform float4 _AO_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _Lighting)
                UNITY_DEFINE_INSTANCED_PROP( float, _Brightness)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _Toononoff)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float3 normalDir : TEXCOORD3;
                LIGHTING_COORDS(4,5)
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float _Lighting_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Lighting );
                float node_2154 = dot(lightDirection,i.normalDir);
                float _Brightness_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Brightness );
                float node_3145 = (max(_Lighting_var,step(0.0,node_2154))*_Brightness_var);
                float _Toononoff_var = lerp( node_3145, (node_3145+node_2154), UNITY_ACCESS_INSTANCED_PROP( Props, _Toononoff ) );
                float3 finalColor = float3(_Toononoff_var,_Toononoff_var,_Toononoff_var);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
