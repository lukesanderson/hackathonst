﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bank : MonoBehaviour
{
    public int foods;
    public int maxFoods = 40;

    public static Bank instance;

    public Image fillImage;

    Transform bankchild;
    Vector3 ogscale;

    private void Awake()
    {
        instance = this;
        bankchild = transform.GetChild(0);
        ogscale = bankchild.transform.localScale;
    }

    public void Deposit()
    {
        foods++;

        GameManager.instance.Vibrate(MoreMountains.NiceVibrations.HapticTypes.MediumImpact);

        fillImage.fillAmount = (float)foods / (float)maxFoods;

        if (foods >= maxFoods)
            GameManager.instance.GameWon();

        bankchild.transform.localScale = Vector3.one * 1.5f;
    }

    private void Update()
    {
        bankchild.transform.localScale = Vector3.Lerp(bankchild.transform.localScale, ogscale, Time.deltaTime * 14f);
    }




}
