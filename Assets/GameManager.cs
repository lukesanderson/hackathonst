﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using UnityEngine.SceneManagement;
using Doozy.Engine.UI;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public UIView startView;
    public UIView gameView;

    public UIView winView;

    public GameObject foodprefab;

    bool gameStarted;

    Movement[] allMovements;

    public List<Transform> childs;

    private void Awake()
    {
        instance = this;
        allMovements = FindObjectsOfType<Movement>();
        foreach (Movement m in allMovements)
        {
            m.enabled = false;
        }

        foreach(Transform child in transform)
        {
            childs.Add(child);
        }
    }

    private void Start()
    {
        for (int i = 0; i < 15; i++)
        {
            SpawnFoodItem();
        }

    }

    public void StartGame()
    {
        startView.Hide();
        gameView.Show();

        gameStarted = true;
        InvokeRepeating(nameof(SpawnFoodItem), 1f, 1f);

        foreach (Movement m in allMovements)
        {
            m.enabled = true;
        }

    }

    public void Vibrate(HapticTypes h)
    {
        MMVibrationManager.Haptic(h);
    }

    public void GameWon()
    {
        gameView.Hide();
        winView.Show();
        foreach(Movement m in FindObjectsOfType<Movement>())
        {
            m.enabled = false;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reset();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            SpawnFoodItem();
        }
    }

    public void Reset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SpawnFoodItem()
    {
        Instantiate(foodprefab, childs[Random.Range(0,childs.Count)].position, Quaternion.identity);
    }

   
}
